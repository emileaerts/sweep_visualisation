import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import h5py

# from pybinding.repository import graphene
from dataclasses import dataclass


@dataclass
class SpecialPoint:
    point: list
    label: str


def get_label_dict(k_path, specials_points_list):

    label_dict = {}

    for special_point in specials_points_list:
        point_ids = np.flatnonzero((k_path == special_point.point).all(1))

        for point_id in point_ids:
            label_dict[point_id] = special_point.label

    return label_dict


def special_points(num_super):
    """Calculates Gamma, M and K special points for superlattice mini Brillouin zone

    :param
    num_super : int
        Number of unit cell repetitions in supercell

    :return
    special_points_list : list of SpecialPoint
        List of SpecialPoint that contains information about location in reciprocal space and label for plotting
    """

    a = 0.24595  # graphene unit cell length

    Gamma = SpecialPoint([0, 0], r'$\Gamma_S$')
    M = SpecialPoint([0, 2 * np.pi / (np.sqrt(3) * num_super * a)], '$M_S$')
    K = SpecialPoint([2 * np.pi / (3 * num_super * a), 2 * np.pi / (np.sqrt(3) * num_super * a)], '$K_S$')

    special_points_list = [Gamma, M, K]
    return special_points_list


def load_files(filenames):
    """load hdf5 files into a list"""
    h5files = []

    for filename in filenames:
        h5files.append(h5py.File(filename, 'r'))

    return h5files


def energy_bands(potential_value, potential_range, hf):
    """The parametrized function to be plotted"""

    potential_idx = int((potential_value - np.min(potential_range)) * (len(potential_range) - 1))
    energy = hf.get('energy_{}'.format(potential_idx))

    return np.array(energy)


def get_filename(lattice_type, num_super, shift):

    if shift:
        filename = '{0}_{1}.h5'.format(lattice_type, num_super)
    else:
        filename = '{0}_{1}_noshift.h5'.format(lattice_type, num_super)
    return filename


def compare_bands(lattice_types, num_supers, shifts):

    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.subplots_adjust(bottom=0.25)

    lattice_type_1, lattice_type_2 = lattice_types
    num_super_1, num_super_2 = num_supers
    shift_1, shift_2 = shifts

    filename_1 = get_filename(lattice_type_1, num_super_1, shift_1)
    filename_2 = get_filename(lattice_type_2, num_super_2, shift_2)
    hf1 = h5py.File(filename_1, 'r')
    hf2 = h5py.File(filename_2, 'r')

    potential_range_1 = np.array(hf1.get('potential_range'))
    potential_range_2 = np.array(hf2.get('potential_range'))

    if (potential_range_1 != potential_range_2).all():
        raise ValueError('To compare band structures potential ranges should be the same')

    potential_range = potential_range_1

    init_potential = np.min(potential_range)
    lines1 = ax1.plot(energy_bands(init_potential, potential_range, hf1), 'k.', markersize=1)
    lines2 = ax2.plot(energy_bands(init_potential, potential_range, hf2), 'k.', markersize=1)

    ax1.set_title(lattice_type_1)
    ax2.set_title(lattice_type_2)

    k_path = np.array(hf1.get('k_path'))
    special_points_list = special_points(num_super_1)
    label_dict = get_label_dict(k_path, special_points_list)
    ax1 = add_labels(ax1, label_dict)
    ax2 = add_labels(ax2, label_dict)

    ax_pot = plt.axes([0.25, 0.1, 0.65, 0.03])  # these numbers only adjust positioning of slider in figure

    potential_slider = Slider(
        ax=ax_pot,
        label='Onsite potential [eV]',
        valmin=np.min(potential_range),
        valmax=np.max(potential_range),
        valinit=init_potential,
        valstep=(np.max(potential_range) - np.min(potential_range)) / (len(potential_range) - 1)
    )

    def update(val):
        bands1 = energy_bands(potential_slider.val, potential_range, hf1)
        bands2 = energy_bands(potential_slider.val, potential_range, hf2)

        for band, line in zip(bands1.T, lines1):
            line.set_ydata(band)

        for band, line in zip(bands2.T, lines2):
            line.set_ydata(band)

        fig.canvas.draw_idle()

    potential_slider.on_changed(update)  # register the update function with each slider

    return fig, (ax1, ax2), potential_slider


def add_labels(ax, label_dict):

    ax.set_xticks(list(label_dict.keys()))
    ax.set_xticklabels(list(label_dict.values()))
    ax.set_ylabel('Energy [eV]')

    for idx in list(label_dict.keys()):
        ax.axvline(idx, alpha=0.3, color='k')

    return ax


def plot_bands(lattice_type, num_super, shift):

    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.25)

    filename = get_filename(lattice_type, num_super, shift)
    hf = h5py.File(filename, 'r')

    potential_range = np.array(hf.get('potential_range'))
    init_potential = np.min(potential_range)

    lines = ax.plot(energy_bands(init_potential, potential_range, hf), 'k.', markersize=1)

    k_path = np.array(hf.get('k_path'))
    special_points_list = special_points(num_super)
    label_dict = get_label_dict(k_path, special_points_list)
    ax = add_labels(ax, label_dict)

    ax_pot = plt.axes([0.25, 0.1, 0.65, 0.03])  # these numbers only adjust positioning of slider in figure

    potential_slider = Slider(
        ax=ax_pot,
        label='Onsite potential [eV]',
        valmin=np.min(potential_range),
        valmax=np.max(potential_range),
        valinit=init_potential,
        valstep=(np.max(potential_range) - np.min(potential_range)) / (len(potential_range) - 1)
    )

    # The function to be called anytime a slider's value changes
    def update(val):
        bands = energy_bands(potential_slider.val, potential_range, hf)

        for band, line in zip(bands.T, lines):
            line.set_ydata(band)

        fig.canvas.draw_idle()

    potential_slider.on_changed(update)  # register the update function with each slider

    return fig, ax, potential_slider


if __name__ == '__main__':
    #
    lattice_type = "kagome"
    num_super = 210
    shift = False

    fig, ax, slider = plot_bands(lattice_type, num_super, shift)
    ax.set_title('{0} {1}x{1} superlattice band structure'.format(lattice_type, num_super))
    plt.show()

    # lattice_types = ["kagome", "triangular"]
    # num_supers = [210, 210]
    # shifts = [True, True]
    #
    # fig, axes, slider = compare_bands(lattice_types, num_supers, shifts)
    # plt.show()
